#include "Analysis.h"
#include "ConfigSvc.h"
#include "Logger.h"
#include "ConfigSvc.h"
#include "EventBase.h"
#include "Logger.h"
#include <TTreeReader.h>

#include "Histograms.h"
#include "HistSvc.h"


Histograms::Histograms(EventBase *eventBase, ConfigSvc *config, HistSvc *hists) {
  m_event = eventBase;
  m_conf = config;
  m_hists = hists;
  INFO("Histogram Setup");
}

Histograms::~Histograms() {}

void Histograms::Histogram_OD_Pulses(std::string base_path,
                                     int id) {

    m_hists->BookFillHist(base_path + "/pulseArea_phd",
                          4000., 0., 4000.,
                          (*m_event->m_odHGPulses)->pulseArea_phd[id]);
    m_hists->BookFillHist(base_path + "/coincidence",
                          120., 0., 120.,
                          (*m_event->m_odHGPulses)->coincidence[id]);
  return;
}


void Histograms::Histogram_OD_Pulses(std::string base_path,
                                     std::vector<int> pulse_ids) {

  for (auto &&id : pulse_ids) {

    Histogram_OD_Pulses(base_path,
                        id);
  }
  return;
}


void Histograms::OD_Positions(std::string base_path, int id) {

  float theta;
  float z_cm_raw;
  float z_cm;
  float z_pmt;
  float theta_pmt;
  float r_cm;
  float x_cm;
  float x_mm;
  float y_cm;
  float y_mm;
  int pmt_id;

  theta = (*m_event->m_odHGPulses)->pulseTheta[id]; //*PI/180.;
  z_cm = (*m_event->m_odHGPulses)->pulseZPosition_cm[id];
  r_cm = (*m_event->m_odHGPulses)->pulseZPosition_cm[id];
  // X-Y and R
  x_mm = 0.0;
  y_mm = 0.0;
  for (int ch_id = 0;
       ch_id < (*m_event->m_odHGPulses)->chPulseArea_phd[id].size(); ++ch_id) {
    pmt_id = (*m_event->m_odHGPulses)->chID[id][ch_id] -
             800; // OD PMT count starts at 800
    x_mm += (*m_event->m_odHGPulses)->chPulseArea_phd[id][ch_id] *
            g_od_pmt_x[pmt_id];
    y_mm += (*m_event->m_odHGPulses)->chPulseArea_phd[id][ch_id] *
            g_od_pmt_y[pmt_id];
  }
  x_mm = x_mm / (*m_event->m_odHGPulses)->pulseArea_phd[id];
  y_mm = y_mm / (*m_event->m_odHGPulses)->pulseArea_phd[id];
  x_cm = x_mm / 10.;
  y_cm = y_mm / 10.;
  r_cm = sqrt(x_cm * x_cm + y_cm * y_cm);

  // Z
  z_cm_raw = -70 * (*m_event->m_odHGPulses)->pulseZPosition[id] + 319.046;
  z_cm = 4.67 * z_cm_raw - 299.40;

  // Theta
  theta = (*m_event->m_odHGPulses)->pulseTheta[id];

  // In pure PMT position
  theta_pmt = (*m_event->m_odHGPulses)->pulseLadderPosition[id];
  z_pmt = (*m_event->m_odHGPulses)->pulseZPosition[id];

  // All Positions
  m_hists->BookFillHist(base_path + "/r_z", 1000, 0, 300, 1000,
                        -200., 350., r_cm, z_cm);
  m_hists->BookFillHist(base_path + "/x_y", 1000, -280, 280, 1000,
                        -280, 280, x_cm, y_cm);
}

void Histograms::Neutron_Capture(std::string base_path, double t0,
                                 std::vector<int> pulse_ids,
                                 bool process_position) {

  int pre_t0_id{-1};
  float pre_t0_phd{-1.};
  int coinc_t0_id{-1};
  float coinc_t0_phd{-1.};
  int post_t0_id{-1};
  float post_t0_phd{-1.};
  std::string base_path;
  double time_diff;

  for (auto &&id : pulse_ids) {

    time_diff = (*m_event->m_odHGPulses)->pulseStartTime_ns[id] - t0;

    if (time_diff < 400) {
      hist_base = base_path + "/Pre_400ns";
      if ((*m_event->m_odHGPulses)->pulseArea_phd[id] > pre_t0_phd) {
        pre_t0_id = id;
      }
    }
    else if (time_diff > 400) {
      hist_base = base_path + "/Post_400ns";
      if ((*m_event->m_odHGPulses)->pulseArea_phd[id] > post_t0_phd) {
        post_t0_id = id;
      }
      m_hists->BookFillHist(
          hist_base + "/Capture_Time_us",
          2000, 0., 2000.,
          time_diff / 1000);
    }
    else {
      hist_base = base_path + "/Coincidence_400ns";
      if ((*m_event->m_odHGPulses)->pulseArea_phd[id] > coinc_t0_phd) {
        coinc_t0_id = id;
      }
    }
    m_hists->BookFillHist(hist_base + "/pulseArea_phd",
                          4000., 0., 4000.,
                          (*m_event->m_odHGPulses)->pulseArea_phd[id]);
    m_hists->BookFillHist(hist_base + "/coincidence",
                          120., 0., 120.,
                          (*m_event->m_odHGPulses)->coincidence[id]);

    m_hists->BookFillHist(
        base_path + "/TimeDifference_us",
        2000, -2000., 2000.,
        time_diff / 1000);

    if (process_position) {
      OD_Positions(hist_base, id);
    }
  }

  if (post_t0_id > -1) {
    m_hists->BookFillHist(base_path + "/Post_400ns/Largest/pulseArea_phd",
                          4000., 0., 4000.,
                          (*m_event->m_odHGPulses)->pulseArea_phd[post_t0_id]);
    m_hists->BookFillHist(base_path + "/Post_400ns/Largest/coincidence",
                          120., 0., 120.,
                          (*m_event->m_odHGPulses)->coincidence[post_t0_id]);
    time_diff = (*m_event->m_odHGPulses)->pulseStartTime_ns[post_t0_id] - t0;
    m_hists->BookFillHist(
        base_path + "/Post_400ns/Largest/TimeDifference_us",
        2000, 0., 2000.,
        time_diff / 1000);
    m_hists->BookFillHist(base_path + "/Post_400ns/Largest/phd_vs_time",
                          80, 0, 2000,
                          200, 0., 2000.,
                          (*m_event->m_odHGPulses)->pulseArea_phd[post_t0_id], time_diff);
    OD_Positions(bae_path + "/Post_400ns/Largest", post_t0_id);
  }
}


