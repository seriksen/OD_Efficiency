#ifndef AmLi_Neutron_Capture_H
#define AmLi_Neutron_Capture_H

#include "Analysis.h"
#include "EventBase.h"
#include "Selector.h"
#include "Histograms.h"

#include <TTreeReader.h>
#include <string>

class SkimSvc;

class AmLi_Neutron_Capture : public Analysis {

public:
  /// Constructor
  AmLi_Neutron_Capture();
  /// Destructor
  ~AmLi_Neutron_Capture();
  /// Called once before the event loop
  void Initialize();
  /// Called once per event
  void Execute();
  /// Called once after event loop
  void Finalize();

protected:

  /// Give access to parameters defined in the Applier.config
  ConfigSvc *m_conf;
  /// Give access to Selector
  Selector *selector;
  /// Give access to Histogram fillers
  Histograms *histograms;

  std::vector<int> od_pulses_all;
  std::vector<int> od_pulses_noise;
  std::vector<int> od_pulses_large;
};

#endif
