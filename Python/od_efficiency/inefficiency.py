"""

"""

import tqdm


def calculate_inefficiency(weights, denominator):
    efficiency = []
    for i in tqdm.tqdm(range(len(weights))):
        weights_to_here = sum(weights[:i])
        efficiency.append(weights_to_here * 100 / denominator)

    return efficiency
