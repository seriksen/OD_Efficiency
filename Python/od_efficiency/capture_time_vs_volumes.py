import uproot as up

import numpy as np
import awkward as ak
import ROOT
import os

locations = ["AluminumSealantInInnerVesselFlange",
             "AngledNeutronTubeCaps",
             "AngledPVCNeutronTube",
             "AnodeGridHolder",
             "AnodeGridWire",
             "AnodePhysGrid",
             "AnodePhysGridSpacer",
             "BoltsInInnerVesselFlange",
             "BoltsInOuterVesselBottomFlange",
             "BoltsInOuterVesselUpperFlange",
             "BottomConduit",
             "BottomFrontTitaniumPlate",
             "BottomGridHolder",
             "BottomGridWire",
             "BottomPMTBase",
             "BottomPMTBody",
             "BottomPMTCable",
             "BottomPMTCeramic",
             "BottomPMTDynode",
             "BottomPMTDynodeInsulator",
             "BottomPMTFlashing",
             "BottomPMTPhotocathode",
             "BottomPMTRod",
             "BottomPMTVacuum",
             "BottomPMTWindow",
             "BottomPTFELiner",
             "BottomPhysGrid",
             "BottomReflector",
             "BottomSkinPMTBody",
             "BottomSkinPMTPhotocathode",
             "BottomSkinPMTVacuum",
             "BottomSkinPMTWindow",
             "BottomTruss",
             "BottomXeSkinPTFELiner",
             "CathodeGridHolder",
             "CathodeGridWire",
             "CathodePhysGrid",
             "CenterReflector",
             "DomePMT1",
             "DomePMT2",
             "DomePMT3",
             "DomePMT4",
             "DomePMT5",
             "DomePMT6",
             "DomeSkinPMTBody",
             "DomeSkinPMTFlashing",
             "DomeSkinPMTPhotocathode",
             "DomeSkinPMTVacuum",
             "DomeSkinPMTWindow",
             "ElastomerSealantInInnerVesselFlange",
             "ElastomerSealantInOuterVesselBottomFlange",
             "ElastomerSealantInOuterVesselUpperFlange",
             "FoamDisplacer",
             "FoamInsulation",
             "ForwardFieldRing",
             "GaseousSkinXenon",
             "GaseousSkinXenonBank",
             "GateGridHolder",
             "GateGridInLiquid",
             "GateGridWire",
             "GatePhysGrid",
             "GatePhysGridSpacer",
             "GateToSurfPTFEWallBlackInset",
             "HVConduit",
             "HVConduitFeedthrough",
             "HVConduitInnerConeInnerStructure",
             "HVConduitInnerOuterSteelCone",
             "HVConduitVacuumInnerConeStructure",
             "HVConduitVacuumInnerSteelCone",
             "HVHolePTFELinerLiquid",
             "InconelSealantInInnerVesselFlange",
             "InnerGaseousXenon",
             "InnerTitaniumVessel",
             "LiquidSkinXenon",
             "LiquidSkinXenonBank",
             "LiquidXenonTarget",
             "NicomicSealantInInnerVesselFlange",
             "OuterTitaniumVessel",
             "PEEKRing",
             "PEEKRingInGas",
             "PMTAlFlashing",
             "PMTCablingConduit",
             "PTFEConeInGas",
             "PTFELinerLiquid",
             "PTFEWallsInGas",
             "PTFEWallsInLiquid",
             "PVCNeutronTube",
             "ResistorInDriftRegion",
             "ResistorInRFR",
             "ReverseFieldRegion",
             "ReverseFieldRing",
             "ScintillatorCenter",
             "ScintillatorTank",
             "SteelLegs",
             "SteelNeutronTubeCaps",
             "ThermosyphonConduit",
             "TitaniumLegs",
             "TopFrontTitaniumPlate",
             "TopPMTBase",
             "TopPMTBody",
             "TopPMTCeramic",
             "TopPMTDynode",
             "TopPMTDynodeInsulator",
             "TopPMTFlashing",
             "TopPMTPhotocathode",
             "TopPMTRod1",
             "TopPMTRod2",
             "TopPMTRod3",
             "TopPMTVacuum",
             "TopPMTWindow",
             "TopPTFELiner",
             "TopReflector",
             "TopSkinIceCubeTray",
             "TopSkinPMTBody",
             "TopSkinPMTPhotocathode",
             "TopSkinPMTVacuum",
             "TopSkinPMTWindow",
             "TopTruss",
             "Tyvek",
             "VacuumSpace",
             "WaterAndPMTs",
             "WaterInAngledNeutronTube",
             "WaterInNeutronTube",
             "WaterPMTCan",
             "WaterPMTPhotocathode",
             "WaterPMTWindow",
             "WaterTank",
             "subVol"]


def get_time_in_vol(vols, times, vol_of_interest):
    time_in_vol = 0.0
    for i in range(len(vols)):
        if i == 0:
            prev_time = 0.0
        else:
            prev_time = times[i - 1]
        if vols[i] == vol_of_interest:
            time_in_vol += (times[i] - prev_time)

    return time_in_vol


def process_file(file_name, outdir='./'):

    histograms = {}
    for l in locations:
        histograms[l] = ROOT.TH2F("hist_" + l, l, 80, 0, 2000, 10, 0, 1)
    hist_capture_times = ROOT.TH1F("hist_capturetimes", "Capture Time", 80, 0, 2000)

    files = glob.glob(
        '/hdfs/user/ak18773/od_simulations/Neutron_Efficency/Energy_Only/BACCARAT_6.2.14_DER_9.1.0_LZAP_5.4.1'
        '/amli_neutrons_only_csd1_z700mm_fnp/neutron_propagation/*.txt')

    for fi in tqdm.tqdm(files):
        neutron_vols = []
        neutron_times = []
        neutron_procs = []
        with open(fi, 'r') as f:
            vols = []
            times = []
            procs = []
            for i, line in enumerate(f):
                if 'New Event' in line and i > 0:
                    neutron_vols.append(vols)
                    neutron_times.append(times)
                    neutron_procs.append(procs)
                    vols = []
                    procs = []
                    times = []
                else:
                    sl = line.split()
                    vols.append(sl[1])
                    times.append(float(sl[2]))
                    procs.append(sl[3])
        for i in range(len(neutron_vols)):
            if neutron_procs[i][-1] == 'nCapture' and neutron_vols[i][-1] == 'ScintillatorCenter':
                capture_time = neutron_times[i][-1]
                hist_capture_times.Fill(capture_time)
                for l in locations:
                    time = get_time_in_vol(neutron_vols[i], neutron_times[i], l)
                    frac = time / capture_time
                    histograms[l].Fill(capture_time, frac)

    # write histograms
    tfile = ROOT.TFile('capture_times_vs_volume_fractions.root', "RECREATE")

    # Overview plots
    for l in locations:
        if histograms[l].Integral() > 0:
            histograms[l].Write()
    hist_capture_times.Write()
    tfile.Close()
