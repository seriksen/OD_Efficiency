"""
Code for calculating the detection efficiency in simulations using energy depositions


Efficiency = numerator / denominator

numerator = energy deposits above threshold within veto window in the OD or the event is vetoes by the Skin

denominator = single scatters and energy deposit in ROI and fiducial volume
"""

import uproot as up

import numpy as np
import awkward as ak
import ROOT
import os


def process_file(file_name, outdir='./'):
    outfile_name = file_name[file_name.rfind('/') + 1:file_name.rfind('.root')] + '_efficiency.root'

    if os.path.exists(outdir + outfile_name):
        return

    events = up.open(file_name)['Events']

    e_deps = events['deposits.EnergyDeps_keV'].array()
    x = events['deposits.positions_x_mm'].array()
    y = events['deposits.positions_y_mm'].array()
    z = events['deposits.positions_z_mm'].array()
    vols = events['deposits.volumeNames'].array()
    times = events['deposits.times_ns'].array()
    pids = events['deposits.pids'].array()
    parent_x = events['primaries.position_X_mm'].array()
    parent_y = events['primaries.position_Y_mm'].array()
    parent_z = events['primaries.position_Z_mm'].array()

    # Overview plots
    h_n_events_all_0 = ROOT.TH1F("N_Events", "N Events", 2, 0, 2)

    h_lxt_r_z_all = ROOT.TH2F("All_LXe_R_Z", "R vs Z, All LXT", 100, 0, 100, 250, -10, 160)
    h_lxt_energy_total_all = ROOT.TH1F("All_LXe_Edep", "Energy All LXT", 2000, 0, 2000)
    h_n_events_all = ROOT.TH1F("N_Events_Processed", "N Events", 2, 0, 2)
    h_lxt_r_sigma_all = ROOT.TH1F("All_Sigma_R", "sigma r All LXT", 100, 0, 10)
    h_lxt_z_sigma_all = ROOT.TH1F("All_Sigma_Z", "sigma z All LXT", 100, 0, 10)
    h_gdcapture_time = ROOT.TH1F("GdCaptureTime", "Neutron capture time", 2000, 0, 2000)
    h_gdcapture_time_finner = ROOT.TH1F("GdCaptureTime", "Neutron capture time", 80, 0, 2000)
    h_gdcapture_time_ss = ROOT.TH1F("GdCaptureTime_SS", "Neutron capture time", 2000, 0, 2000)
    h_gdcapture_time_no_ss = ROOT.TH1F("GdCaptureTime_No_SS", "Neutron capture time", 2000, 0, 2000)
    h_scint_edep = ROOT.TH1F("scint_edep", "Scint E", 1500, 0, 15000)

    # Single Scatter Plots
    h_lxt_r_z_ss = ROOT.TH2F("With_SS_LXe_R_Z", "R vs Z, SS LXT", 100, 0, 100, 250, -10, 160)
    h_lxt_energy_total_ss = ROOT.TH1F("With_SS_LXe_Edep", "E, SS LXT", 2000, 0, 2000)
    h_n_events_ss = ROOT.TH1F("With_SS_N_Events", "N Events", 2, 0, 2)
    h_lxt_start_pos_ss = ROOT.TH2F("With_SS_Start_Pos_R_Z", "Start_Pos, SS LXT", 100, 0, 100, 250, -10, 160)
    h_time_difference_ss_od_only = ROOT.TH1F("With_SS_td_od_only_0keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_od_only_100 = ROOT.TH1F("With_SS_td_od_only_100keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_od_only_200 = ROOT.TH1F("With_SS_td_od_only_200keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_skin_only = ROOT.TH1F("With_SS_td_skin_only_0keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_skin_only_100 = ROOT.TH1F("With_SS_td_skin_only_100keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_skin_only_200 = ROOT.TH1F("With_SS_td_skin_only_200keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_all_veto = ROOT.TH1F("With_SS_td_all_veto_0keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_all_veto_100 = ROOT.TH1F("With_SS_td_veto_100keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_all_veto_200 = ROOT.TH1F("With_SS_td_veto_200keV", "Time Diff", 2000, 0, 2000)

    h_time_difference_ss_od_only_100_total = ROOT.TH1F("With_SS_td_od_only_100keV_total", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_od_only_200_total = ROOT.TH1F("With_SS_td_od_only_200keV_total", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_skin_only_100_total = ROOT.TH1F("With_SS_td_skin_only_100keV_total", "Time Diff", 2000, 0,
                                                         2000)
    h_time_difference_ss_skin_only_200_total = ROOT.TH1F("With_SS_td_skin_only_200keV_total", "Time Diff", 2000, 0,
                                                         2000)
    h_time_difference_ss_all_veto_100_total = ROOT.TH1F("With_SS_td_veto_100keV_total", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_all_veto_200_total = ROOT.TH1F("With_SS_td_veto_200keV_total", "Time Diff", 2000, 0, 2000)

    ## Fid cut
    h_lxt_r_z_ss_fid = ROOT.TH2F("With_SS_FID_LXe_R_Z", "R vs Z, SS and FID LXT", 100, 0, 100, 250, -10, 160)
    h_lxt_energy_total_ss_fid = ROOT.TH1F("With_SS_FID_LXe_Edep", "E, SS and FID LXT", 2000, 0, 2000)
    h_n_events_ss_fid = ROOT.TH1F("With_SS_FID_N_Events", "N Events", 2, 0, 2)
    h_time_difference_ss_fid_od_only = ROOT.TH1F("With_SS_FID_td_od_only_0keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_fid_od_only_100 = ROOT.TH1F("With_SS_FID_td_od_only_100keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_fid_od_only_200 = ROOT.TH1F("With_SS_FID_td_od_only_200keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_fid_skin_only = ROOT.TH1F("With_SS_FID_td_skin_only_0keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_fid_skin_only_100 = ROOT.TH1F("With_SS_FID_td_skin_only_100keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_fid_skin_only_200 = ROOT.TH1F("With_SS_FID_td_skin_only_200keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_fid_all_veto = ROOT.TH1F("With_SS_FID_td_all_veto_0keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_fid_all_veto_100 = ROOT.TH1F("With_SS_FID_td_veto_100keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_fid_all_veto_200 = ROOT.TH1F("With_SS_FID_td_veto_200keV", "Time Diff", 2000, 0, 2000)

    h_time_difference_ss_fid_od_only_100_total = ROOT.TH1F("With_SS_FID_td_od_only_100keV_total", "Time Diff", 2000, 0,
                                                           2000)
    h_time_difference_ss_fid_od_only_200_total = ROOT.TH1F("With_SS_FID_td_od_only_200keV_total", "Time Diff", 2000, 0,
                                                           2000)
    h_time_difference_ss_fid_skin_only_100_total = ROOT.TH1F("With_SS_FID_td_skin_only_100keV_total", "Time Diff", 2000,
                                                             0, 2000)
    h_time_difference_ss_fid_skin_only_200_total = ROOT.TH1F("With_SS_FID_td_skin_only_200keV_total", "Time Diff", 2000,
                                                             0, 2000)
    h_time_difference_ss_fid_all_veto_100_total = ROOT.TH1F("With_SS_FID_td_veto_100keV_total", "Time Diff", 2000, 0,
                                                            2000)
    h_time_difference_ss_fid_all_veto_200_total = ROOT.TH1F("With_SS_FID_td_veto_200keV_total", "Time Diff", 2000, 0,
                                                            2000)

    ## ROI cut
    h_lxt_r_z_ss_fid_roi = ROOT.TH2F("With_SS_ROI_LXe_R_Z", "R vs Z, SS and FID LXT", 100, 0, 100, 250, -10, 160)
    h_lxt_energy_total_ss_fid_roi = ROOT.TH1F("With_SS_ROI_LXe_Edep", "E, SS and FID LXT", 2000, 0, 2000)
    h_n_events_ss_fid_roi = ROOT.TH1F("With_SS_ROI_N_Events", "N Events", 2, 0, 2)
    h_time_difference_ss_fid_od_only_roi = ROOT.TH1F("With_SS_ROI_td_od_only_0keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_fid_od_only_100_roi = ROOT.TH1F("With_SS_ROI_td_od_only_100keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_fid_od_only_200_roi = ROOT.TH1F("With_SS_ROI_td_od_only_200keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_fid_skin_only_roi = ROOT.TH1F("With_SS_ROI_td_skin_only_0keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_fid_skin_only_100_roi = ROOT.TH1F("With_SS_ROI_td_skin_only_100keV", "Time Diff", 2000, 0,
                                                           2000)
    h_time_difference_ss_fid_skin_only_200_roi = ROOT.TH1F("With_SS_ROI_td_skin_only_200keV", "Time Diff", 2000, 0,
                                                           2000)
    h_time_difference_ss_fid_all_veto_roi = ROOT.TH1F("With_SS_ROI_td_all_veto_0keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_fid_all_veto_100_roi = ROOT.TH1F("With_SS_ROI_td_veto_100keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_ss_fid_all_veto_200_roi = ROOT.TH1F("With_SS_ROI_td_veto_200keV", "Time Diff", 2000, 0, 2000)

    h_time_difference_ss_fid_od_only_100_roi_total = ROOT.TH1F("With_SS_ROI_td_od_only_100keV_total", "Time Diff", 1000,
                                                               0, 2000)
    h_time_difference_ss_fid_od_only_200_roi_total = ROOT.TH1F("With_SS_ROI_td_od_only_200keV_total", "Time Diff", 1000,
                                                               0, 2000)
    h_time_difference_ss_fid_skin_only_100_roi_total = ROOT.TH1F("With_SS_ROI_td_skin_only_100keV_total", "Time Diff",
                                                                 2000, 0,
                                                                 2000)
    h_time_difference_ss_fid_skin_only_200_roi_total = ROOT.TH1F("With_SS_ROI_td_skin_only_200keV_total", "Time Diff",
                                                                 2000, 0,
                                                                 2000)
    h_time_difference_ss_fid_all_veto_100_roi_total = ROOT.TH1F("With_SS_ROI_td_veto_100keV_total", "Time Diff", 1000,
                                                                0, 2000)
    h_time_difference_ss_fid_all_veto_200_roi_total = ROOT.TH1F("With_SS_ROI_td_veto_200keV_total", "Time Diff", 1000,
                                                                0, 2000)

    # No Single Scatter Plots
    h_lxt_r_z_no_ss = ROOT.TH2F("No_SS_LXe_R_Z", "R vs Z, SS LXT", 100, 0, 100, 250, -10, 160)
    h_n_events_no_ss = ROOT.TH1F("No_SS_N_Events", "N Events", 2, 0, 2)
    h_time_difference_no_ss_od_only = ROOT.TH1F("No_SS_td_od_only_0keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_od_only_100 = ROOT.TH1F("No_SS_td_od_only_100keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_od_only_200 = ROOT.TH1F("No_SS_td_od_only_200keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_skin_only = ROOT.TH1F("No_SS_td_skin_only_0keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_skin_only_100 = ROOT.TH1F("No_SS_td_skin_only_100keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_skin_only_200 = ROOT.TH1F("No_SS_td_skin_only_200keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_all_veto = ROOT.TH1F("No_SS_td_all_veto_0keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_all_veto_100 = ROOT.TH1F("No_SS_td_veto_100keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_all_veto_200 = ROOT.TH1F("No_SS_td_veto_200keV", "Time Diff", 2000, 0, 2000)

    h_time_difference_no_ss_od_only_100_total = ROOT.TH1F("No_SS_td_od_only_100keV_total", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_od_only_200_total = ROOT.TH1F("No_SS_td_od_only_200keV_total", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_skin_only_100_total = ROOT.TH1F("No_SS_td_skin_only_100keV_total", "Time Diff", 2000, 0,
                                                            2000)
    h_time_difference_no_ss_skin_only_200_total = ROOT.TH1F("No_SS_td_skin_only_200keV_total", "Time Diff", 2000, 0,
                                                            2000)
    h_time_difference_no_ss_all_veto_100_total = ROOT.TH1F("No_SS_td_veto_100keV_total", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_all_veto_200_total = ROOT.TH1F("No_SS_td_veto_200keV_total", "Time Diff", 2000, 0, 2000)

    ## Fid cut
    h_lxt_r_z_no_ss_fid = ROOT.TH2F("No_SS_FID_LXe_R_Z", "R vs Z, SS and FID LXT", 100, 0, 100, 250, -10, 160)
    h_time_difference_no_ss_fid_od_only = ROOT.TH1F("No_SS_FID_td_od_only_0keV", "Time Diff", 2000, 0, 2000)
    h_n_events_no_ss_fid = ROOT.TH1F("No_SS_FID_N_Events", "N Events", 2, 0, 2)
    h_time_difference_no_ss_fid_od_only_100 = ROOT.TH1F("No_SS_FID_td_od_only_100keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_fid_od_only_200 = ROOT.TH1F("No_SS_FID_td_od_only_200keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_fid_skin_only = ROOT.TH1F("No_SS_FID_td_skin_only_0keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_fid_skin_only_100 = ROOT.TH1F("No_SS_FID_td_skin_only_100keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_fid_skin_only_200 = ROOT.TH1F("No_SS_FID_td_skin_only_200keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_fid_all_veto = ROOT.TH1F("No_SS_FID_td_all_veto_0keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_fid_all_veto_100 = ROOT.TH1F("No_SS_FID_td_veto_100keV", "Time Diff", 2000, 0, 2000)
    h_time_difference_no_ss_fid_all_veto_200 = ROOT.TH1F("No_SS_FID_td_veto_200keV", "Time Diff", 2000, 0, 2000)

    h_time_difference_no_ss_fid_od_only_100_total = ROOT.TH1F("No_SS_FID_td_od_only_100keV_total", "Time Diff", 2000, 0,
                                                              2000)
    h_time_difference_no_ss_fid_od_only_200_total = ROOT.TH1F("No_SS_FID_td_od_only_200keV_total", "Time Diff", 2000, 0,
                                                              2000)
    h_time_difference_no_ss_fid_skin_only_100_total = ROOT.TH1F("No_SS_FID_td_skin_only_100keV_total", "Time Diff",
                                                                2000, 0, 2000)
    h_time_difference_no_ss_fid_skin_only_200_total = ROOT.TH1F("No_SS_FID_td_skin_only_200keV_total", "Time Diff",
                                                                2000, 0, 2000)
    h_time_difference_no_ss_fid_all_veto_100_total = ROOT.TH1F("No_SS_FID_td_veto_100keV_total", "Time Diff", 2000, 0,
                                                               2000)
    h_time_difference_no_ss_fid_all_veto_200_total = ROOT.TH1F("No_SS_FID_td_veto_200keV_total", "Time Diff", 2000, 0,
                                                               2000)

    # Loop over files
    for i in range(len(e_deps)):
        h_n_events_all_0.Fill(1)
        if len(vols[i]) > 0:
            h_n_events_all.Fill(1)

            # Get this event
            d_x = x[i] / 10
            d_y = y[i] / 10
            d_z = z[i] / 10
            d_e = e_deps[i]
            d_vol = vols[i]
            d_time = times[i]
            p_x = parent_x[i][0] / 10
            p_y = parent_y[i][0] / 10
            p_z = parent_z[i][0] / 10

            # neutron capture time
            pid = pids[i]
            for j, p in enumerate(pid):
                if p == 1000641580:
                    h_gdcapture_time.Fill(d_time[j] / 1000)
                    h_gdcapture_time_finner.Fill(d_time[j] / 1000)

            # Where are the deposits
            lxt = np.where(d_vol == 'LiquidXenonTarget')
            scint = np.where(d_vol == 'ScintillatorCenter')
            skin = np.where(d_vol == 'LiquidSkinXenon')

            od_e = d_e[scint]
            od_e_tot = sum(od_e)
            od_times = d_time[scint]

            skin_e = d_e[skin]
            skin_e_tot = sum(skin_e)
            skin_times = d_time[skin]

            od_is_100 = np.where(od_e > 100)[0]
            od_is_200 = np.where(od_e > 200)[0]
            skin_is_100 = np.where(skin_e > 100)[0]
            skin_is_200 = np.where(skin_e > 200)[0]

            #####################
            ## Single Scatters ##
            #####################
            if np.size(lxt) > 0:
                lx_x = d_x[lxt]
                lx_y = d_y[lxt]
                lx_z = d_z[lxt]
                lx_e = d_e[lxt]
                lx_time = d_time[lxt][0]
                lx_r = np.sqrt(lx_x ** 2 + lx_y ** 2)
                lx_e_tot = sum(lx_e)

                lx_r_e = energy_weighted_mean(lx_r, lx_e)
                lx_z_e = energy_weighted_mean(lx_z, lx_e)

                is_ss, r_sigma, z_sigma = ss_cut(lx_r, lx_z, lx_e)

                h_lxt_r_z_all.Fill(lx_r_e, lx_z_e)
                h_lxt_energy_total_all.Fill(lx_e_tot)
                h_lxt_r_sigma_all.Fill(r_sigma)
                h_lxt_z_sigma_all.Fill(z_sigma)

                # If Single Scatter
                if is_ss:

                    h_n_events_ss.Fill(1)
                    h_lxt_r_z_ss.Fill(lx_r_e, lx_z_e)
                    h_lxt_energy_total_ss.Fill(lx_e_tot)
                    h_lxt_start_pos_ss.Fill(np.sqrt(p_x ** 2 + p_y ** 2), p_z)

                    # OD Only
                    if od_e_tot > 0:
                        h_scint_edep.Fill(od_e_tot)
                        time_difference = np.abs(lx_time - od_times[0])
                        h_time_difference_ss_od_only.Fill(time_difference / 1000)
                        if np.size(od_is_100) > 0:
                            time_difference_100 = np.abs(lx_time - od_times[od_is_100[0]])
                            h_time_difference_ss_od_only_100.Fill(time_difference_100 / 1000)
                            if np.size(od_is_200) > 0:
                                time_difference_200 = np.abs(lx_time - od_times[od_is_200[0]])
                                h_time_difference_ss_od_only_200.Fill(time_difference_200 / 1000)
                        if od_e_tot > 100:
                            time_difference_100 = np.abs(lx_time - od_times[0])
                            h_time_difference_ss_od_only_100_total.Fill(time_difference_100 / 1000)
                            if od_e_tot > 200:
                                time_difference_200 = np.abs(lx_time - od_times[0])
                                h_time_difference_ss_od_only_200_total.Fill(time_difference_200 / 1000)
                    if skin_e_tot > 0:
                        time_difference = np.abs(lx_time - skin_times[0])
                        h_time_difference_ss_skin_only.Fill(time_difference / 1000)
                        if np.size(skin_is_100) > 0:
                            time_difference_100 = np.abs(lx_time - skin_times[skin_is_100[0]])
                            h_time_difference_ss_skin_only_100.Fill(time_difference_100 / 1000)
                            if np.size(skin_is_200) > 0:
                                time_difference_200 = np.abs(lx_time - skin_times[skin_is_200[0]])
                                h_time_difference_ss_skin_only_200.Fill(time_difference_200 / 1000)
                        if skin_e_tot > 100:
                            time_difference_100 = np.abs(lx_time - skin_times[0])
                            h_time_difference_ss_skin_only_100_total.Fill(time_difference_100 / 1000)
                            if skin_e_tot > 200:
                                time_difference_200 = np.abs(lx_time - skin_times[0])
                                h_time_difference_ss_skin_only_200_total.Fill(time_difference_200 / 1000)
                    if skin_e_tot > 0 or od_e_tot > 0:
                        veto_time = get_lower_time(skin_times, od_times, skin_e_tot, od_e_tot, 0)
                        time_difference = np.abs(lx_time - veto_time)
                        h_time_difference_ss_all_veto.Fill(time_difference / 1000)
                        if np.size(skin_is_100) > 0 or np.size(od_is_100) > 0:
                            veto_time = get_lower_time_2(skin_times, od_times, skin_is_100, od_is_100)
                            time_difference_100 = np.abs(lx_time - veto_time)
                            h_time_difference_ss_all_veto_100.Fill(time_difference_100 / 1000)
                            if np.size(od_is_200) > 0 or np.size(od_is_200) > 0:
                                veto_time = get_lower_time_2(skin_times, od_times, skin_is_200, od_is_200)
                                time_difference_200 = np.abs(lx_time - veto_time)
                                h_time_difference_ss_all_veto_200.Fill(time_difference_200 / 1000)
                        if skin_e_tot > 100 or od_e_tot > 100:
                            veto_time = get_lower_time(skin_times, od_times, skin_e_tot, od_e_tot, 100)
                            time_difference_100 = np.abs(lx_time - veto_time)
                            h_time_difference_ss_all_veto_100_total.Fill(time_difference_100 / 1000)
                            if skin_e_tot > 100 or od_e_tot > 200:
                                veto_time = get_lower_time_3(skin_times, od_times, skin_e_tot, od_e_tot, 100, 200)
                                time_difference_200 = np.abs(lx_time - veto_time)
                                h_time_difference_ss_all_veto_200_total.Fill(time_difference_200 / 1000)

                    # neutron capture time
                    pid = pids[i]
                    for j, p in enumerate(pid):
                        if p == 1000641580:
                            h_gdcapture_time_ss.Fill(d_time[j] / 1000)

                    # check if in ROI
                    # May need to include Reverse Field Region in this
                    is_wimp_roi = wimp_roi_cut(lx_e_tot)

                    is_fid = fiducial_cut(lx_r_e, lx_z_e)

                    # if Fiducial Volume
                    if is_fid:
                        h_n_events_ss_fid.Fill(1)
                        h_lxt_r_z_ss_fid.Fill(lx_r_e, lx_z_e)
                        h_lxt_energy_total_ss_fid.Fill(lx_e_tot)

                        # OD Only
                        if od_e_tot > 0:
                            time_difference = np.abs(lx_time - od_times[0])
                            h_time_difference_ss_fid_od_only.Fill(time_difference / 1000)
                            if np.size(od_is_100) > 0:
                                time_difference_100 = np.abs(lx_time - od_times[od_is_100[0]])
                                h_time_difference_ss_fid_od_only_100.Fill(time_difference_100 / 1000)
                                if np.size(od_is_200) > 0:
                                    time_difference_200 = np.abs(lx_time - od_times[od_is_200[0]])
                                    h_time_difference_ss_fid_od_only_200.Fill(time_difference_200 / 1000)
                            if od_e_tot > 100:
                                time_difference_100 = np.abs(lx_time - od_times[0])
                                h_time_difference_ss_fid_od_only_100_total.Fill(time_difference_100 / 1000)
                                if od_e_tot > 200:
                                    time_difference_200 = np.abs(lx_time - od_times[0])
                                    h_time_difference_ss_fid_od_only_200_total.Fill(time_difference_200 / 1000)
                        if skin_e_tot > 0:
                            time_difference = np.abs(lx_time - skin_times[0])
                            h_time_difference_ss_fid_skin_only.Fill(time_difference / 1000)
                            if np.size(skin_is_100) > 0:
                                time_difference_100 = np.abs(lx_time - skin_times[skin_is_100[0]])
                                h_time_difference_ss_fid_skin_only_100.Fill(time_difference_100 / 1000)
                                if np.size(skin_is_200) > 0:
                                    time_difference_200 = np.abs(lx_time - skin_times[skin_is_200[0]])
                                    h_time_difference_ss_fid_skin_only_200.Fill(time_difference_200 / 1000)
                            if skin_e_tot > 100:
                                time_difference_100 = np.abs(lx_time - skin_times[0])
                                h_time_difference_ss_fid_skin_only_100_total.Fill(time_difference_100 / 1000)
                                if skin_e_tot > 200:
                                    time_difference_200 = np.abs(lx_time - skin_times[0])
                                    h_time_difference_ss_fid_skin_only_200_total.Fill(time_difference_200 / 1000)
                        if skin_e_tot > 0 or od_e_tot > 0:
                            veto_time = get_lower_time(skin_times, od_times, skin_e_tot, od_e_tot, 0)
                            time_difference = np.abs(lx_time - veto_time)
                            h_time_difference_ss_fid_all_veto.Fill(time_difference / 1000)
                            if np.size(skin_is_100) > 0 or np.size(od_is_100) > 0:
                                veto_time = get_lower_time_2(skin_times, od_times, skin_is_100, od_is_100)
                                time_difference_100 = np.abs(lx_time - veto_time)
                                h_time_difference_ss_fid_all_veto_100.Fill(time_difference_100 / 1000)
                                if np.size(od_is_200) > 0 or np.size(od_is_200) > 0:
                                    veto_time = get_lower_time_2(skin_times, od_times, skin_is_200, od_is_200)
                                    time_difference_200 = np.abs(lx_time - veto_time)
                                    h_time_difference_ss_fid_all_veto_200.Fill(time_difference_200 / 1000)
                            if skin_e_tot > 100 or od_e_tot > 100:
                                veto_time = get_lower_time(skin_times, od_times, skin_e_tot, od_e_tot, 100)
                                time_difference_100 = np.abs(lx_time - veto_time)
                                h_time_difference_ss_fid_all_veto_100_total.Fill(time_difference_100 / 1000)
                                if skin_e_tot > 100 or od_e_tot > 200:
                                    veto_time = get_lower_time_3(skin_times, od_times, skin_e_tot, od_e_tot, 100, 200)
                                    time_difference_200 = np.abs(lx_time - veto_time)
                                    h_time_difference_ss_fid_all_veto_200_total.Fill(time_difference_200 / 1000)

                        if is_wimp_roi:
                            h_n_events_ss_fid_roi.Fill(1)
                            h_lxt_r_z_ss_fid_roi.Fill(lx_r_e, lx_z_e)
                            h_lxt_energy_total_ss_fid_roi.Fill(lx_e_tot)
                            if od_e_tot > 0:
                                time_difference = np.abs(lx_time - od_times[0])
                                h_time_difference_ss_fid_od_only_roi.Fill(time_difference / 1000)
                                if np.size(od_is_100) > 0:
                                    time_difference_100 = np.abs(lx_time - od_times[od_is_100[0]])
                                    h_time_difference_ss_fid_od_only_100_roi.Fill(time_difference_100 / 1000)
                                    if np.size(od_is_200) > 0:
                                        time_difference_200 = np.abs(lx_time - od_times[od_is_200[0]])
                                        h_time_difference_ss_fid_od_only_200_roi.Fill(time_difference_200 / 1000)
                                if od_e_tot > 100:
                                    time_difference_100 = np.abs(lx_time - od_times[0])
                                    h_time_difference_ss_fid_od_only_100_roi_total.Fill(time_difference_100 / 1000)
                                    if od_e_tot > 200:
                                        time_difference_200 = np.abs(lx_time - od_times[0])
                                        h_time_difference_ss_fid_od_only_200_roi_total.Fill(time_difference_200 / 1000)
                            if skin_e_tot > 0:
                                time_difference = np.abs(lx_time - skin_times[0])
                                h_time_difference_ss_fid_skin_only_roi.Fill(time_difference / 1000)
                                if np.size(skin_is_100) > 0:
                                    time_difference_100 = np.abs(lx_time - skin_times[skin_is_100[0]])
                                    h_time_difference_ss_fid_skin_only_100_roi.Fill(time_difference_100 / 1000)
                                    if np.size(skin_is_200) > 0:
                                        time_difference_200 = np.abs(lx_time - skin_times[skin_is_200[0]])
                                        h_time_difference_ss_fid_skin_only_200_roi.Fill(time_difference_200 / 1000)
                                if skin_e_tot > 100:
                                    time_difference_100 = np.abs(lx_time - skin_times[0])
                                    h_time_difference_ss_fid_skin_only_100_roi_total.Fill(time_difference_100 / 1000)
                                    if skin_e_tot > 200:
                                        time_difference_200 = np.abs(lx_time - skin_times[0])
                                        h_time_difference_ss_fid_skin_only_200_roi_total.Fill(
                                            time_difference_200 / 1000)
                            if skin_e_tot > 0 or od_e_tot > 0:
                                veto_time = get_lower_time(skin_times, od_times, skin_e_tot, od_e_tot, 0)
                                time_difference = np.abs(lx_time - veto_time)
                                h_time_difference_ss_fid_all_veto_roi.Fill(time_difference / 1000)
                                if np.size(skin_is_100) > 0 or np.size(od_is_100) > 0:
                                    veto_time = get_lower_time_2(skin_times, od_times, skin_is_100, od_is_100)
                                    time_difference_100 = np.abs(lx_time - veto_time)
                                    h_time_difference_ss_fid_all_veto_100_roi.Fill(time_difference_100 / 1000)
                                    if np.size(od_is_200) > 0 or np.size(od_is_200) > 0:
                                        veto_time = get_lower_time_2(skin_times, od_times, skin_is_200, od_is_200)
                                        time_difference_200 = np.abs(lx_time - veto_time)
                                        h_time_difference_ss_fid_all_veto_200_roi.Fill(time_difference_200 / 1000)
                                if skin_e_tot > 100 or od_e_tot > 100:
                                    veto_time = get_lower_time(skin_times, od_times, skin_e_tot, od_e_tot, 100)
                                    time_difference_100 = np.abs(lx_time - veto_time)
                                    h_time_difference_ss_fid_all_veto_100_roi_total.Fill(time_difference_100 / 1000)
                                    if skin_e_tot > 100 or od_e_tot > 200:
                                        veto_time = get_lower_time_3(skin_times, od_times, skin_e_tot, od_e_tot, 100,
                                                                     200)
                                        time_difference_200 = np.abs(lx_time - veto_time)
                                        h_time_difference_ss_fid_all_veto_200_roi_total.Fill(time_difference_200 / 1000)

            # Not Single Scatters
            else:
                p_r = np.sqrt(p_x ** 2 + p_y ** 2)

                h_lxt_r_z_no_ss.Fill(p_r, p_z)
                h_n_events_no_ss.Fill(1)

                # neutron capture time
                pid = pids[i]
                for j, p in enumerate(pid):
                    if p == 1000641580:
                        h_gdcapture_time_no_ss.Fill(d_time[j] / 1000)

                # OD Only
                if od_e_tot > 0:
                    time_difference = od_times[0]
                    h_time_difference_no_ss_od_only.Fill(time_difference / 1000)
                    if np.size(od_is_100) > 0:
                        time_difference_100 = od_times[od_is_100[0]]
                        h_time_difference_no_ss_od_only_100.Fill(time_difference_100 / 1000)
                        if np.size(od_is_200) > 0:
                            time_difference_200 = od_times[od_is_200[0]]
                            h_time_difference_no_ss_od_only_200.Fill(time_difference_200 / 1000)
                    if od_e_tot > 100:
                        time_difference_100 = od_times[0]
                        h_time_difference_no_ss_od_only_100_total.Fill(time_difference_100 / 1000)
                        if od_e_tot > 200:
                            time_difference_200 = od_times[0]
                            h_time_difference_no_ss_od_only_200_total.Fill(time_difference_200 / 1000)
                if skin_e_tot > 0:
                    time_difference = skin_times[0]
                    h_time_difference_no_ss_skin_only.Fill(time_difference / 1000)
                    if np.size(skin_is_100) > 0:
                        time_difference_100 = skin_times[skin_is_100[0]]
                        h_time_difference_no_ss_skin_only_100.Fill(time_difference_100 / 1000)
                        if np.size(skin_is_200) > 0:
                            time_difference_200 = skin_times[skin_is_200[0]]
                            h_time_difference_no_ss_skin_only_200.Fill(time_difference_200 / 1000)
                    if skin_e_tot > 100:
                        time_difference_100 = skin_times[0]
                        h_time_difference_no_ss_skin_only_100_total.Fill(time_difference_100 / 1000)
                        if skin_e_tot > 200:
                            time_difference_200 = skin_times[0]
                            h_time_difference_no_ss_skin_only_200_total.Fill(time_difference_200 / 1000)
                if skin_e_tot > 0 or od_e_tot > 0:
                    veto_time = get_lower_time(skin_times, od_times, skin_e_tot, od_e_tot, 0)
                    time_difference = veto_time
                    h_time_difference_no_ss_all_veto.Fill(time_difference / 1000)
                    if np.size(skin_is_100) > 0 or np.size(od_is_100) > 0:
                        veto_time = get_lower_time_2(skin_times, od_times, skin_is_100, od_is_100)
                        time_difference_100 = veto_time
                        h_time_difference_no_ss_all_veto_100.Fill(time_difference_100 / 1000)
                        if np.size(od_is_200) > 0 or np.size(od_is_200) > 0:
                            veto_time = get_lower_time_2(skin_times, od_times, skin_is_200, od_is_200)
                            time_difference_200 = veto_time
                            h_time_difference_no_ss_all_veto_200.Fill(time_difference_200 / 1000)
                    if skin_e_tot > 100 or od_e_tot > 100:
                        veto_time = get_lower_time(skin_times, od_times, skin_e_tot, od_e_tot, 100)
                        time_difference_100 = veto_time
                        h_time_difference_no_ss_all_veto_100_total.Fill(time_difference_100 / 1000)
                        if skin_e_tot > 100 or od_e_tot > 200:
                            veto_time = get_lower_time_3(skin_times, od_times, skin_e_tot, od_e_tot, 100, 200)
                            time_difference_200 = veto_time
                            h_time_difference_no_ss_all_veto_200_total.Fill(time_difference_200 / 1000)
                is_fid = fiducial_cut(p_r, p_z)
                if is_fid:
                    h_n_events_no_ss_fid.Fill(1)
                    h_lxt_r_z_no_ss_fid.Fill(p_r, p_z)

                    # OD Only
                    if od_e_tot > 0:
                        time_difference = od_times[0]
                        h_time_difference_no_ss_fid_od_only.Fill(time_difference / 1000)
                        if np.size(od_is_100) > 0:
                            time_difference_100 = od_times[od_is_100[0]]
                            h_time_difference_no_ss_fid_od_only_100.Fill(time_difference_100 / 1000)
                            if np.size(od_is_200) > 0:
                                time_difference_200 = od_times[od_is_200[0]]
                                h_time_difference_no_ss_fid_od_only_200.Fill(time_difference_200 / 1000)
                        if od_e_tot > 100:
                            time_difference_100 = od_times[0]
                            h_time_difference_no_ss_fid_od_only_100_total.Fill(time_difference_100 / 1000)
                            if od_e_tot > 200:
                                time_difference_200 = od_times[0]
                                h_time_difference_no_ss_fid_od_only_200_total.Fill(time_difference_200 / 1000)
                    if skin_e_tot > 0:
                        time_difference = skin_times[0]
                        h_time_difference_no_ss_fid_skin_only.Fill(time_difference / 1000)
                        if np.size(skin_is_100) > 0:
                            time_difference_100 = skin_times[skin_is_100[0]]
                            h_time_difference_no_ss_fid_skin_only_100.Fill(time_difference_100 / 1000)
                            if np.size(skin_is_200) > 0:
                                time_difference_200 = skin_times[skin_is_200[0]]
                                h_time_difference_no_ss_fid_skin_only_200.Fill(time_difference_200 / 1000)
                        if skin_e_tot > 100:
                            time_difference_100 = skin_times[0]
                            h_time_difference_no_ss_fid_skin_only_100_total.Fill(time_difference_100 / 1000)
                            if skin_e_tot > 200:
                                time_difference_200 = skin_times[0]
                                h_time_difference_no_ss_fid_skin_only_200_total.Fill(time_difference_200 / 1000)
                    if skin_e_tot > 0 or od_e_tot > 0:
                        veto_time = get_lower_time(skin_times, od_times, skin_e_tot, od_e_tot, 0)
                        time_difference = veto_time
                        h_time_difference_no_ss_fid_all_veto.Fill(time_difference / 1000)
                        if np.size(skin_is_100) > 0 or np.size(od_is_100) > 0:
                            veto_time = get_lower_time_2(skin_times, od_times, skin_is_100, od_is_100)
                            time_difference_100 = veto_time
                            h_time_difference_no_ss_fid_all_veto_100.Fill(time_difference_100 / 1000)
                            if np.size(od_is_200) > 0 or np.size(od_is_200) > 0:
                                veto_time = get_lower_time_2(skin_times, od_times, skin_is_200, od_is_200)
                                time_difference_200 = veto_time
                                h_time_difference_no_ss_fid_all_veto_200.Fill(time_difference_200 / 1000)
                        if skin_e_tot > 100 or od_e_tot > 100:
                            veto_time = get_lower_time(skin_times, od_times, skin_e_tot, od_e_tot, 100)
                            time_difference_100 = veto_time
                            h_time_difference_no_ss_fid_all_veto_100_total.Fill(time_difference_100 / 1000)
                            if skin_e_tot > 100 or od_e_tot > 200:
                                veto_time = get_lower_time_3(skin_times, od_times, skin_e_tot, od_e_tot, 100, 200)
                                time_difference_200 = veto_time
                                h_time_difference_no_ss_fid_all_veto_200_total.Fill(time_difference_200 / 1000)

    # write histograms
    outfile_name = file_name[file_name.rfind('/') + 1:file_name.rfind('.root')] + '_efficiency.root'
    tfile = ROOT.TFile(outdir + outfile_name, "RECREATE")

    # N Events
    h_n_events_all_0.Write()
    h_n_events_all.Write()
    h_n_events_ss.Write()
    h_n_events_ss_fid.Write()
    h_n_events_ss_fid_roi.Write()
    h_n_events_no_ss.Write()
    h_n_events_no_ss_fid.Write()

    # Overview plots
    h_lxt_r_z_all.Write()
    h_lxt_energy_total_all.Write()
    h_lxt_r_sigma_all.Write()
    h_lxt_z_sigma_all.Write()
    h_gdcapture_time.Write()
    h_gdcapture_time_ss.Write()
    h_gdcapture_time_no_ss.Write()

    # Single Scatter Plots
    h_lxt_r_z_ss.Write()
    h_lxt_energy_total_ss.Write()
    h_lxt_start_pos_ss.Write()
    h_time_difference_ss_od_only.Write()
    h_time_difference_ss_od_only_100.Write()
    h_time_difference_ss_od_only_200.Write()
    h_time_difference_ss_skin_only.Write()
    h_time_difference_ss_skin_only_100.Write()
    h_time_difference_ss_skin_only_200.Write()
    h_time_difference_ss_all_veto.Write()
    h_time_difference_ss_all_veto_100.Write()
    h_time_difference_ss_all_veto_200.Write()
    h_time_difference_ss_od_only_100_total.Write()
    h_time_difference_ss_od_only_200_total.Write()
    h_time_difference_ss_skin_only_100_total.Write()
    h_time_difference_ss_skin_only_200_total.Write()
    h_time_difference_ss_all_veto_100_total.Write()
    h_time_difference_ss_all_veto_200_total.Write()

    ## Fid cut
    h_lxt_r_z_ss_fid.Write()
    h_lxt_energy_total_ss_fid.Write()
    h_time_difference_ss_fid_od_only.Write()
    h_time_difference_ss_fid_od_only_100.Write()
    h_time_difference_ss_fid_od_only_200.Write()
    h_time_difference_ss_fid_skin_only.Write()
    h_time_difference_ss_fid_skin_only_100.Write()
    h_time_difference_ss_fid_skin_only_200.Write()
    h_time_difference_ss_fid_all_veto.Write()
    h_time_difference_ss_fid_all_veto_100.Write()
    h_time_difference_ss_fid_all_veto_200.Write()
    h_time_difference_ss_fid_od_only_100_total.Write()
    h_time_difference_ss_fid_od_only_200_total.Write()
    h_time_difference_ss_fid_skin_only_100_total.Write()
    h_time_difference_ss_fid_skin_only_200_total.Write()
    h_time_difference_ss_fid_all_veto_100_total.Write()
    h_time_difference_ss_fid_all_veto_200_total.Write()

    ## ROI cut
    h_lxt_r_z_ss_fid_roi.Write()
    h_lxt_energy_total_ss_fid_roi.Write()
    h_time_difference_ss_fid_od_only_roi.Write()
    h_time_difference_ss_fid_od_only_100_roi.Write()
    h_time_difference_ss_fid_od_only_200_roi.Write()
    h_time_difference_ss_fid_skin_only_roi.Write()
    h_time_difference_ss_fid_skin_only_100_roi.Write()
    h_time_difference_ss_fid_skin_only_200_roi.Write()
    h_time_difference_ss_fid_all_veto_roi.Write()
    h_time_difference_ss_fid_all_veto_100_roi.Write()
    h_time_difference_ss_fid_all_veto_200_roi.Write()
    h_time_difference_ss_fid_od_only_100_roi_total.Write()
    h_time_difference_ss_fid_od_only_200_roi_total.Write()
    h_time_difference_ss_fid_skin_only_100_roi_total.Write()
    h_time_difference_ss_fid_skin_only_200_roi_total.Write()
    h_time_difference_ss_fid_all_veto_100_roi_total.Write()
    h_time_difference_ss_fid_all_veto_200_roi_total.Write()

    # No Single Scatter Plots
    h_lxt_r_z_no_ss.Write()
    h_time_difference_no_ss_od_only.Write()
    h_time_difference_no_ss_od_only_100.Write()
    h_time_difference_no_ss_od_only_200.Write()
    h_time_difference_no_ss_skin_only.Write()
    h_time_difference_no_ss_skin_only_100.Write()
    h_time_difference_no_ss_skin_only_200.Write()
    h_time_difference_no_ss_all_veto.Write()
    h_time_difference_no_ss_all_veto_100.Write()
    h_time_difference_no_ss_all_veto_200.Write()
    h_time_difference_no_ss_od_only_100_total.Write()
    h_time_difference_no_ss_od_only_200_total.Write()
    h_time_difference_no_ss_skin_only_100_total.Write()
    h_time_difference_no_ss_skin_only_200_total.Write()
    h_time_difference_no_ss_all_veto_100_total.Write()
    h_time_difference_no_ss_all_veto_200_total.Write()
    ## Fid cut
    h_lxt_r_z_no_ss_fid.Write()
    h_time_difference_no_ss_fid_od_only.Write()
    h_time_difference_no_ss_fid_od_only_100.Write()
    h_time_difference_no_ss_fid_od_only_200.Write()
    h_time_difference_no_ss_fid_skin_only.Write()
    h_time_difference_no_ss_fid_skin_only_100.Write()
    h_time_difference_no_ss_fid_skin_only_200.Write()
    h_time_difference_no_ss_fid_all_veto.Write()
    h_time_difference_no_ss_fid_all_veto_100.Write()
    h_time_difference_no_ss_fid_all_veto_200.Write()
    h_time_difference_no_ss_fid_od_only_100_total.Write()
    h_time_difference_no_ss_fid_od_only_200_total.Write()
    h_time_difference_no_ss_fid_skin_only_100_total.Write()
    h_time_difference_no_ss_fid_skin_only_200_total.Write()
    h_time_difference_no_ss_fid_all_veto_100_total.Write()
    h_time_difference_no_ss_fid_all_veto_200_total.Write()

    h_scint_edep.Write()
    h_gdcapture_time_finner.Write()

    tfile.Close()


def ss_cut(rs, zs, energies):
    """
    Determine if deposits are close enough to be called a signal scatter

    :param rs: deposit X locations
    :type rs: ak.Array[float] or float
    :param zs: deposit Z locations
    :type zs: ak.Array[float] or float
    :param energies: deposit energies
    :type energies: ak.Array[float] or float
    :return: signel scatter result, r sigma, z sigma
    :rtype: list[bool, float, float]
    """

    r_sigma = energy_weighted_sigma(rs, energies)

    z_sigma = energy_weighted_sigma(zs, energies)

    if r_sigma < 3.0 and z_sigma < 0.2:
        result = True
    else:
        result = False
    return result, r_sigma, z_sigma


def energy_weighted_mean(values, energies):
    """
    Weight a value based upon the energy

    :param values:
    :type values: float, ak.Array[float]
    :param energies:
    :type energies: float, ak.Array[float]
    :return:
    :rtype: float
    """

    return sum(values * energies) / sum(energies)


def energy_weighted_sigma(values, energies):
    """

    :param values:
    :type values:
    :param energies:
    :type energies:
    :return:
    :rtype:
    """
    if len(values) == 1:
        sigma = 0.0
    else:
        weighted_mean = energy_weighted_mean(values, energies)
        energy_sq = sum(energies * energies)
        distance = sum(((values - weighted_mean) ** 2) * energies)
        total_e = sum(energies)
        sigma = np.sqrt(distance * total_e / (total_e ** 2 - energy_sq))

    return sigma


def wimp_roi_cut(energy):
    """
    Apply WIMP ROI cut: 6keV - 30keV

    :param energy:
    :type energy: float
    :return:
    :rtype: bool
    """
    if 6.0 < energy < 30.0:
        return True
    else:
        return False


def fiducial_cut(r, z):
    """
    Apply Fiducial cut.
    Simple Rectangle to match TDR era

    :param r: scatter r position in cm
    :type r: float
    :param z: scatter z position in cm
    :type z: float
    :return: True if r and z are in the fiducial region
    :rtype: bool
    """

    if r < 68.8 and 1.5 < z < 132.1:
        return True
    else:
        return False


def get_lower_time(skin_times, od_times, skin_energy, od_energy, energy):
    time = -10.0
    skin_time = -2
    od_time = -2
    if skin_energy > energy:
        skin_time = skin_times[0]
    if od_energy > energy:
        od_time = od_times[0]

    if skin_time > -1 and od_time > -1:
        if skin_time > od_time:
            time = od_time
        else:
            time = skin_time
    elif skin_time > -1 > od_time:
        time = skin_time
    elif od_time > -1 > skin_time:
        time = od_time

    return time


def get_lower_time_2(skin_times, od_times, skin_ids, od_ids):
    time = -10.0
    skin_time = 0.0
    od_time = 0.0
    if np.size(skin_ids) > 0 and np.size(od_ids) > 0:
        skin_time = skin_times[skin_ids[0]]
        od_time = od_times[od_ids[0]]
        if od_time > skin_time:
            time = skin_time
        else:
            time = od_time
    elif np.size(skin_ids) > 0:
        time = skin_times[skin_ids[0]]
    elif np.size(od_ids) > 0:
        time = od_times[od_ids[0]]

    return time


def get_lower_time_3(skin_times, od_times, skin_energy, od_energy, skin_req, od_req):
    time = -10.0
    skin_time = -2
    od_time = -2
    if skin_energy > skin_req:
        skin_time = skin_times[0]
    if od_energy > od_req:
        od_time = od_times[0]

    if skin_time > -1 and od_time > -1:
        if skin_time > od_time:
            time = od_time
        else:
            time = skin_time
    elif skin_time > -1 > od_time:
        time = skin_time
    elif od_time > -1 > skin_time:
        time = od_time

    return time
